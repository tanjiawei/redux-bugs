import axios from "axios";
import * as actions from "../api";

const api = ({ getState, dispatch }) => (next) => async (action) => {
  if (action.type !== actions.apiCallBegan().type) return next(action);
  const { url, method, data, onStart, onError, onSuccess } = action.payload;

  if (onStart) dispatch({ type: onStart });

  next(action);

  try {
    const response = await axios({
      baseURL: "http://localhost:9001/api",
      method,
      url,
      data,
    });
    dispatch(actions.apiCallSuccess(response.data));
    if (onSuccess) return dispatch({ type: onSuccess, payload: response.data });
  } catch (error) {
    dispatch(actions.apiCallFailed(error.message));
    if (onError) return dispatch({ type: onError, payload: error.message });
  }
};

export default api;
