import configureStore from "./store/configureStore";
import { addBug, loadBugs, assignBugToUser } from "./store/bugs";
import { userAdded } from "./store/users";

const store = configureStore();
store.dispatch(loadBugs());
store.dispatch(addBug({ description: "e" }));
store.dispatch(userAdded({ name: "Westley" }));
store.dispatch(assignBugToUser(20, 4));
