import React, { Component } from "react";
import { connect } from "react-redux";
import { loadBugs, resolveBug, getUnresolvedBugs } from "../store/bugs";

class Bugs extends Component {
  componentDidMount() {
    this.props.loadBugs();
  }

  render() {
    const { bugs, resolveBug } = this.props;
    return (
      <React.Fragment>
        <h2>All Bugs Unresolved (class)</h2>
        <ul>
          {bugs.map((bug) => (
            <li key={bug.id} className="py-1">
              {bug.description}{" "}
              <button
                type="button"
                className="btn btn-primary btn-sm"
                onClick={() => resolveBug(bug.id)}
              >
                Resolve
              </button>
            </li>
          ))}
        </ul>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => ({ bugs: getUnresolvedBugs(state) });

const mapDispatchToProps = (dispatch) => ({
  loadBugs: () => dispatch(loadBugs()),
  resolveBug: (id) => dispatch(resolveBug(id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Bugs);
