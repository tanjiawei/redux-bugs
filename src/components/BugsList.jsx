import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { loadBugs, getUnresolvedBugs, resolveBug } from "../store/bugs";

const BugsList = () => {
  const dispatch = useDispatch();
  const bugs = useSelector(getUnresolvedBugs);

  useEffect(() => {
    dispatch(loadBugs());
  }, []);

  return (
    <React.Fragment>
      <h2>All Bugs Unresolved (functional)</h2>
      <ul>
        {bugs.map((bug) => (
          <li key={bug.id} className="py-1">
            {bug.description}{" "}
            <button
              type="button"
              className="btn btn-primary btn-sm"
              onClick={() => dispatch(resolveBug(bug.id))}
            >
              Resolve
            </button>
          </li>
        ))}
      </ul>
    </React.Fragment>
  );
};

export default BugsList;
