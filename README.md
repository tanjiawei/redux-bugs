# Redux Demo

A small application that demostrates the redux in action.

## Getting Started and Notes

For simplicity of running and reviewing this application in action, the mock/fake backend has been included in the SAME file. This is purely for simplicity and demostration, and such logics `SHOULD NOT` be applied in the real world applications.

### Prerequisites

You need to have `node` to run this application. Please head over to [Node](https://nodejs.org/en/) page to download and install it. The `npm` command will be used in this application.

### Installing

Run below command to install the dependencies and development dependencies.

```
npm i
```

After installation, start the program by running a concurrent command which will start the fake backend server and then the frontend application.

```
npm start
```

### About tests

These tests test the output of the Redux ecosystem as a whole. They test on the output by inputting simple information and compare the behavior of the redux components as a whole instead of testing on the individual implementation of each store, action or reducer.

## Running the tests

```
npm test
```

### And coding style tests

Explain what these tests test and why

```
Give an example
```

## Deployment

Add additional notes about how to deploy this on a live system

## Built With

For development:

- @babel/core
- @babel/preset-env
- @types/jest
- babel-jest
- concurrently
- jest
- webpack
- webpack-cli
- webpack-dev-server

For production:

- @reduxjs/toolkit
- axios
- body-parser
- cors
- express
- immer
- immutable
- lodash
- moment
- redux
- redux-devtools-extension
- reselect

## Authors

- **Westley Tan** - _Subsequent work_

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details

## Acknowledgments

- [Code with Mosh](https://codewithmosh.com/)
